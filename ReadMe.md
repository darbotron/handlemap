"handle map" is a container which stores objects and allows them to be accessed via a handle

Handles are structs which go invalid WRT the HandleMap if the object they are a handle for is removed (as opposed to going null/keeping an object around forever like an object reference).

Handles have a second useful property: in the same way objects can be associated across multiple arrays by index, handles can be used to associate objects across multiple maps.

To do this we need a "parent" HandleMap responsible for creating the handles and a "shadow HandleMap" responsible for maintaing a shadowed mapping to its own content using the handles created by the parent.

This allows, for example, a ControllerManager to manage a HandleMap of hardware controllers, and a separate InputManager to handle a collection of associated arbitrary input (re)mappings from hardware input to game actions accessed using the same handle as for their associated hardware controller created by ControllerManager's HandleMap.

HandleMapBase and HandleMap< T > are implemented with extensive testing (would like to move to NUnit or similar at some point).

In addition there are the "shadowed types" with a little less testing but still good coverage:

- HandleMapShadowerBase: constructed with a HandleMapBase instance to "shadow"; is the base of:
- HandleMapListener< T >: just gets notifications of a HandleMap< T > changing.
- HandleMapShadower< TSource, TShadowed >: maintains a shadowed collection of TShadowed corresponding to the parent HandleMap's collection of TSource by handle and provides callbacks to manipulate it when new TSource instaces are added / removed.

Now works properly with value types, also allowing a default value to be specified (e.g. using the defacto standard of Type.Invalid)

Also allows custom Equality comparer for the mapped type.
