﻿//#define TEST_WITH_VALUETYPE
//#define TEST_WITH_SPECIFIEDDEFAULT
//#define TEST_WITH_CUSTOMEQUALITYCOMPARER

using System;
using System.Collections.Generic;

using System.Diagnostics;

using PlatformLayer;

public static class Constants
{
	public const int k_iInvalidIndex = -1;
}

namespace HandleMapperTest
{
	internal class Program
	{
		#if TEST_WITH_VALUETYPE
			public struct Test
			{
				public void Init()
				{
					Data = ++m_dataInitialiser;
				}

				public int Data { get; set; }

				private static int m_dataInitialiser = 0;

				public bool Equals( Test other ) => ( Data == other.Data );

				public static bool operator ==( Test left, Test right ) => left.Equals( right );

				public static bool operator !=( Test left, Test right ) => ( ! left.Equals( right ) );

				public override bool Equals( object other )
				{
					if( other is Test test )
					{
						return Equals( test );
					}
					return false;
				}

				public override int GetHashCode()
				{
					return Data;
				}

				public static Test Invalid = new Test(){ Data = Constants.k_iInvalidIndex };
			}

			static Test CreateTestInstance()
			{
				var valueInstance = new Test();
				valueInstance.Init();
				return valueInstance;
			}

		#else
		
			public class Test
			{
				public Test()
				{
					Data = ++m_dataInitialiser;
				}

				public int Data { get; set; }

				private static int m_dataInitialiser = 0;

				public static Test Invalid = null;
			}

			static Test CreateTestInstance()
			{
				return  new Test();
			}

		#endif

		class CustomTestComparer : EqualityComparer< Test >
		{
			public override bool	Equals		( Test x, Test y )	=> ( x == y );
			public override int		GetHashCode	( Test obj )				=> ( obj.GetHashCode() );
		}


		public class MapListenerTest : HandleMapListener< Test >
		{
			public MapListenerTest( HandleMap< Test > handleMap ) : base( handleMap )
			{}

			protected override void OnJustAddedHandle( HandleMap<Test> handleMap, HandleMapBase.Handle handle )
			{
				Debug.Assert( handleMap.HandleHasValidMapping( handle ) );
				Console.WriteLine( $"JustAdded: {handle.ToString()}" );
			}

			protected override void OnAboutToRemoveHandle( HandleMap<Test> handleMap, HandleMapBase.Handle handle )
			{
				Debug.Assert( handleMap.HandleHasValidMapping( handle ) );

				Console.WriteLine( $"AboutToRemove: {handle.ToString()}" );
			}
		}

		public class MapShadowerTest : HandleMapShadower< Test, int >
		{
			public MapShadowerTest( HandleMap< Test > mapToShadow ) : base ( mapToShadow )
			{}

			protected override void OnJustAddedHandle( HandleMap< Test > handleMap, HandleMapBase.Handle handle, ref int refToShadowedInstanceForHandle )
			{
				if( handleMap.TryGetMappedItem( handle, out var test ) )
				{
					refToShadowedInstanceForHandle = test.Data;
					Console.WriteLine( $"JustAdded: {handle.ToString()} - data: {test.Data} shadow data: {refToShadowedInstanceForHandle}" );

				} else
				{
					Debug.Assert( false );
				}
			}

			protected override void OnAboutToRemoveHandle( HandleMap< Test > handleMap, HandleMapBase.Handle handle, ref int refToShadowedInstanceForHandle )
			{
				if( handleMap.TryGetMappedItem( handle, out var test ) )
				{
					Console.WriteLine( $"AboutToRemove: {handle.ToString()} - data: {test.Data} shadow data: {refToShadowedInstanceForHandle}" );
					Debug.Assert( refToShadowedInstanceForHandle == test.Data );
					refToShadowedInstanceForHandle = 0;

				} else
				{
					Debug.Assert( false );
				}
			}
		}

		const int k_iMaxMappedItems = 10;

		static HandleMap< Test > CreateHandleMap()
		{
			// TEST_WITH_VALUETYPE - makes no difference
				
			#if TEST_WITH_SPECIFIEDDEFAULT
					
				#if TEST_WITH_CUSTOMEQUALITYCOMPARER						
					return new HandleMap< Test >( k_iMaxMappedItems, new CustomTestComparer(), Test.Invalid );
				#else
					return new HandleMap< Test >( k_iMaxMappedItems, Test.Invalid );
				#endif
		
			#elif TEST_WITH_CUSTOMEQUALITYCOMPARER

				return new HandleMap< Test >( k_iMaxMappedItems, new CustomTestComparer() );

			#else

				return new HandleMap< Test >( k_iMaxMappedItems );

			#endif
		}

		private static void Main( string[] args )
		{
			//TestBehaviourOfRefKeyword();

			var testArray = new Test[k_iMaxMappedItems];

			for( var i = 0; i < testArray.Length; ++i )
			{
				testArray[i] = CreateTestInstance();
			}

			////////////////////////////////////////////////////////////
			#region test map in isolation


			RunTest( "empty map", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );

				ValidateEmpty( k_iMaxMappedItems, testMap );
			} );

			RunTest( "fill", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				foreach( var test in testArray )
				{ testMap.AddOrGetExistingHandle( test ); }

				ValidateFull( k_iMaxMappedItems, testMap );
			} );

			RunTest( "fill and clear", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				foreach( var test in testArray )
				{ testMap.AddOrGetExistingHandle( test ); }

				testMap.Clear();

				ValidateEmpty( k_iMaxMappedItems, testMap );
			} );

			RunTest( "add one", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );

				testMap.AddOrGetExistingHandle( testArray[0] );

				Debug.Assert( testMap.Count             == 1 );
				Debug.Assert( testMap.RemainingCapacity == ( k_iMaxMappedItems - 1 ) );
				Debug.Assert( !testMap.IsFull );
			} );

			RunTest( "add and access via returned handle", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				var testZero = testArray[0];
				var handle = testMap.AddOrGetExistingHandle( testZero );

				ValidateHandleFindsObject( testMap, testZero, handle );

			} );

			RunTest( "add and retrieve handle, then access with it", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				var testZero = testArray[0];
				var handle = testMap.AddOrGetExistingHandle( testZero );

				ValidateObjectFindsHandle( testMap, testZero, handle );

			} );

			RunTest( "add A -> retrieve B via handle( A ) -> retrieve handle( B ) -> check vs handle( A )", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				var testZero = testArray[0];
				var handle = testMap.AddOrGetExistingHandle( testZero );

				if( testMap.TryGetMappedItem( handle, out var testZeroTwo ) )
				{
					if( testMap.TryGetHandleForItem( testZeroTwo, out var handleTwo ) )
					{
						Debug.Assert( testZero  == testZeroTwo );
						Debug.Assert( handle    == handleTwo );
					}
					else
					{
						Debug.Assert( false );
					}
				}
				else
				{
					Debug.Assert( false );
				}
			} );

			RunTest( "add and remove via handle", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				var testZero = testArray[0];
				var handle = testMap.AddOrGetExistingHandle( testZero );

				testMap.Remove( handle );

				ValidateEmpty( k_iMaxMappedItems, testMap );
				Debug.Assert( !testMap.HandleHasValidMapping( handle ) );
				Debug.Assert( !testMap.TryGetMappedItem( handle, out var dummy ) );
				Debug.Assert( !testMap.TryGetHandleForItem( testZero, out var dummyHandle ) );
			} );

			RunTest( "add and remove via item", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				var testZero = testArray[0];
				var handle = testMap.AddOrGetExistingHandle( testZero );

				testMap.Remove( testZero );

				ValidateEmpty( k_iMaxMappedItems, testMap );
				Debug.Assert( !testMap.HandleHasValidMapping( handle ) );
				Debug.Assert( !testMap.TryGetMappedItem( handle, out var dummy ) );
				Debug.Assert( !testMap.TryGetHandleForItem( testZero, out var dummyHandle ) );
			} );

			RunTest( "add same > once", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				var testZero = testArray[0];

				var handle = testMap.AddOrGetExistingHandle( testZero );
				var count = testMap.Count;

				var handleTwo = testMap.AddOrGetExistingHandle( testZero );

				Debug.Assert( handle    == handleTwo );
				Debug.Assert( count     == testMap.Count );
			} );


			// add -> remove -> re-add check handle validity
			// same with add -> clear -> re-add
			// n.b. if just add/remove a single one then will always use the same handle slot..
			RunTest( "test generations", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );
				var testZero = testArray[0];

				var handle = testMap.AddOrGetExistingHandle( testZero );
				testMap.Remove( testZero );
				Debug.Assert( !testMap.HandleHasValidMapping( handle ) );

				var handleTwo = testMap.AddOrGetExistingHandle( testZero );
				Debug.Assert( handle    != handleTwo );
				Debug.Assert( !testMap.HandleHasValidMapping( handle ) );
				Debug.Assert( testMap.HandleHasValidMapping( handleTwo ) );

				testMap.Remove( handle ); // old handle
				Debug.Assert( testMap.HandleHasValidMapping( handleTwo ) );

				testMap.Remove( handleTwo );
				Debug.Assert( !testMap.HandleHasValidMapping( handleTwo ) );
			} );


			RunTest( "add and remove willy nilly and check everything is fine", () =>
			{
				var testMap = new HandleMap<Test>( k_iMaxMappedItems );

				ValidateStressTest( k_iMaxMappedItems, testArray, testMap );
			} );


			#endregion //test map in isolation
			////////////////////////////////////////////////////////////
			#region test handle map listener


			RunTest( "test HandleMap listener", () =>
			{
				var testMap			= new HandleMap<Test>( k_iMaxMappedItems );
				var testListener	= new MapListenerTest( testMap );

				ValidateStressTest( k_iMaxMappedItems, testArray, testMap );
			} );


			#endregion test handle map listener
			////////////////////////////////////////////////////////////
			#region test handle map shadow


			RunTest( "test HandleMapShadower", () =>
			{
				var testMap			= new HandleMap<Test>( k_iMaxMappedItems );
				var testMapShadower	= new MapShadowerTest( testMap );

				foreach( var test in testArray )
				{
					var handle = testMap.AddOrGetExistingHandle( test );

					if( testMap.TryGetMappedItem( handle, out var testFromHandle ) )
					{
						if( testMapShadower.TryGetShadowed( handle, out var testData ) )
						{
							Debug.Assert( testFromHandle.Data == testData );
						}
						else
						{
							Debug.Assert( false );
						}
					} else
					{
						Debug.Assert( false );
					}
				}


				var numToRemove		= ( testMap.Count / 2 );
				var randomGenerator	= new Random();

				Console.WriteLine( $"\n## removing {numToRemove} handles at random from {nameof( testMap )} (contains: {testMap.Count})..." );

				for( var i = 0; i < numToRemove; ++i )
				{
					var randomIndex = randomGenerator.Next( 0, testMap.Count );
					var handle		= testMap.GetMappedHandles()[ randomIndex ];

					testMap.Remove( handle );
				}
				Console.WriteLine( $"... removed {numToRemove} handles from {nameof( testMap )} (contains: {testMap.Count}) ##" );


				Console.WriteLine( $"\n## about to clear {nameof( testMap )} (contains: {testMap.Count})..." );
				testMap.Clear();
				Console.WriteLine( $"cleared {nameof( testMap )} ##" );
				
				// THIS EXPECTS AN EMPTY testMap!
				ValidateStressTest( k_iMaxMappedItems, testArray, testMap );
			} );


			#endregion test handle map shadowe
			////////////////////////////////////////////////////////////

			Console.ReadKey();
		}

		private static void ValidateStressTest( int k_iMaxMappedItems, Test[] testArray, HandleMap<Test> testMap )
		{
			var notAddedList = new List<Test>( k_iMaxMappedItems );
			var addedList = new List<HandleMapBase.Handle>( k_iMaxMappedItems );

			foreach( var test in testArray )
			{
				notAddedList.Add( test );
			}

			const int k_iNumLoops = 10;
			var rand = new Random();

			var numToAddEachLoopArray = new int[k_iNumLoops];
			var numToRemoveEachLoopArray = new int[k_iNumLoops];

			for( var i = 0; i < numToAddEachLoopArray.Length; ++i )
			{
				numToAddEachLoopArray[i]      = rand.Next( 1, k_iMaxMappedItems );
				numToRemoveEachLoopArray[i]   = rand.Next( 1, k_iMaxMappedItems );
			}

			// ensure it's full and empty at least once
			numToAddEachLoopArray[rand.Next( 0, numToAddEachLoopArray.Length )] = k_iMaxMappedItems;
			numToRemoveEachLoopArray[rand.Next( 0, numToAddEachLoopArray.Length )] = k_iMaxMappedItems;



			for( var iLoop = 0; iLoop < k_iNumLoops; ++iLoop )
			{
				var numToAdd = Math.Min( numToAddEachLoopArray[iLoop], notAddedList.Count );
				Console.WriteLine( $"## Loop {iLoop} - adding {numToAdd} items ##" );

				for( var i = 0; i < numToAdd; ++i )
				{
					Console.WriteLine( $"->adding {i}th item:" );

					var randomIndex = rand.Next( 0, notAddedList.Count );
					var test = notAddedList[randomIndex];
					notAddedList.RemoveAt( randomIndex );

					var handle = testMap.AddOrGetExistingHandle( test );
					Debug.Assert( !handle.IsInvalid() );
					Debug.Assert( testMap.HandleHasValidMapping( handle ) );

					ValidateHandleFindsObject( testMap, test, handle );
					ValidateObjectFindsHandle( testMap, test, handle );

					addedList.Add( handle );

					Debug.Assert( testMap.Count             == addedList.Count );
					Debug.Assert( testMap.RemainingCapacity == notAddedList.Count );
				}

				var numToRemove = Math.Min( numToRemoveEachLoopArray[iLoop], addedList.Count );
				Console.WriteLine( $"## Loop {iLoop} - removing {numToRemove} items ##" );

				for( var i = 0; i < numToRemove; ++i )
				{
					Console.WriteLine( $"->removing {i}th item:" );

					var randomIndex = rand.Next( 0, addedList.Count );
					var handle = addedList[randomIndex];
					addedList.RemoveAt( randomIndex );

					Debug.Assert( !handle.IsInvalid() );
					Debug.Assert( testMap.HandleHasValidMapping( handle ) );

					if( testMap.TryGetMappedItem( handle, out var test ) )
					{
						testMap.Remove( handle );

						Debug.Assert( !testMap.HandleHasValidMapping( handle ) );

						notAddedList.Add( test );
					}
					else
					{
						Debug.Assert( false );
					}

					Debug.Assert( testMap.Count             == addedList.Count );
					Debug.Assert( testMap.RemainingCapacity == notAddedList.Count );
				}

				Console.WriteLine( "" );
			}

			// remove all objects 
			while( addedList.Count > 0 )
			{
				var handle = addedList[0];
				addedList.RemoveAt( 0 );
				testMap.Remove( handle );

				Debug.Assert( !testMap.HandleHasValidMapping( handle ) );
			}

			ValidateEmpty( k_iMaxMappedItems, testMap );
		}

		private static bool ValidateObjectFindsHandle( HandleMap<Test> testMap, Test testZero, HandleMapBase.Handle handle )
		{
			Debug.Assert( testMap.TryGetHandleForItem( testZero, out var dummyHandle ) );
			if( testMap.TryGetHandleForItem( testZero, out var retrievedHandle ) )
			{
				Debug.Assert( testMap.HandleHasValidMapping( retrievedHandle ) );
				Debug.Assert( handle == retrievedHandle );
				return true;
			}
			return false;
		}

		private static bool ValidateHandleFindsObject( HandleMap<Test> testMap, Test testZero, HandleMapBase.Handle handle )
		{
			Debug.Assert( testMap.HandleHasValidMapping( handle ) );
			Debug.Assert( testMap.TryGetMappedItem( handle, out var dummyTest ) );

			if( testMap.TryGetMappedItem( handle, out var retrievedTest ) )
			{
				Debug.Assert( default( Test )	!= retrievedTest );
				Debug.Assert( testZero			== retrievedTest );
				return true;
			}
			return false;
		}

		private static void RunTest( string testname, Action fnDoTest )
		{
			Console.WriteLine( $"RunningTest {testname} ..." );
			fnDoTest();
			Console.WriteLine( $"... completed ok" );
			Console.WriteLine( "" );
		}

		private static void ValidateFull( int maxMappedItems, HandleMap<Test> testMap )
		{
			Debug.Assert( testMap.Count              == maxMappedItems );
			Debug.Assert( testMap.RemainingCapacity  == 0 );
			Debug.Assert( testMap.IsFull );
		}

		private static void ValidateEmpty( int maxMappedItems, HandleMap<Test> testMap )
		{
			Debug.Assert( testMap.Count              == 0 );
			Debug.Assert( testMap.RemainingCapacity  == maxMappedItems );
			Debug.Assert( !testMap.IsFull );
		}


		private static void DoubleIntRef( ref int refInt )
		{
			refInt *= 2;
		}

		private static void AssignRefRef( ref Test refTest )
		{
			refTest = new Test();
		}

		private static void TestBehaviourOfRefKeyword()
		{
			// test behaviour of 'ref' works as expected

			var intArray = new int[] { 1, 2, 3, 4 };

			for( var i = 0; i < intArray.Length; ++i )
			{
				DoubleIntRef( ref intArray[i] );
			}

			var arrayRefTest = new Test[4];

			for( var i = 0; i < arrayRefTest.Length; ++i )
			{
				AssignRefRef( ref arrayRefTest[i] );
			}
		}

	}
}
