﻿//////////////////////////////////////////////////////////////////////////////
// This source file is part of the "Unity Platform Layer"
//
// File content is © 2018 Darbotron Ltd
//
// It may not be shared, reproduced, or used in any way whatsoever
// without the express permission of Darbotron Ltd
//////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace PlatformLayer
{
	using System;
	using System.Collections.ObjectModel;
	using System.Collections.Generic;
	using HandleMapperTest;
	using Constants = Constants;

	// uses technique described here: https://stackoverflow.com/questions/19135534/c-sharp-equivalent-to-c-friend-keyword/21518037
	// to emulate the C++ concept of "friend"	
	//
	// Part 1: a "dummy" interface as the key to the technique
	// (search for part 2)
	public interface IKey{}

	public abstract class HandleMapBase
	{
		////////////////////////////////////////////////////////////
		#region Handle 


		//
		// handle struct which can only be created by HandleMapBase
		public struct Handle
		{
			////////////////////////////////////////////////////////////
			// functions only callable with THandleMapKey == HandleMap.HandleMapPrivateKey, which only HandleMap can access
			#region friend HandleMap

			// uses technique described here: https://stackoverflow.com/questions/19135534/c-sharp-equivalent-to-c-friend-keyword/21518037
			// to emulate the C++ concept of "friend"	
			//
			// Part 2: API interface functions which require a dummy template param derived from the key and the declaring type
			// (search for part 3)


			//----------------------------------------------------------
			public static Handle CreateHandle< THandleMapKey >( int index, int generation ) where THandleMapKey : HandleMapBase, IKey => new Handle( index, generation );

			//----------------------------------------------------------
			public int GetHandleMapIndex< THandleMapKey >() where THandleMapKey : HandleMapBase, IKey => m_index;

			//----------------------------------------------------------
			public int GetHandleMapGeneration< THandleMapKey >() where THandleMapKey : HandleMapBase, IKey => m_generation;


			#endregion //#region friend HandleMap
			////////////////////////////////////////////////////////////
			#region public


			public const			int		k_invalidHandleIndex		= Constants.k_iInvalidIndex;
			public const			int		k_invalidHandleGeneration	= Constants.k_iInvalidIndex;
			public static readonly	Handle	Invalid						= new Handle( k_invalidHandleIndex, k_invalidHandleGeneration );

			public bool IsInvalid() => this == Invalid;

			public override string ToString() => $"[{GetType().Name}:[index:{m_index}][gen:{m_generation}]]";


			#endregion// public
			////////////////////////////////////////////////////////////
			#region equality


			//----------------------------------------------------------
			public override bool Equals(object obj)
			{
				if( obj is Handle )
				{
					return Equals((Handle) obj );
				}
				return false;
			}

			//----------------------------------------------------------
			public bool Equals( Handle other )
			{
				return (	( m_index		== other.m_index )
						&&	( m_generation	== other.m_generation ) );
			}

			//----------------------------------------------------------
			public override int GetHashCode() => m_index ^ m_generation;

			//----------------------------------------------------------
			public static bool operator ==( Handle lhs, Handle rhs ) => lhs.Equals( rhs );

			//----------------------------------------------------------
			public static bool operator !=( Handle lhs, Handle rhs ) => !lhs.Equals( rhs );


			#endregion // equality
			////////////////////////////////////////////////////////////
			#region private


			//----------------------------------------------------------
			private Handle( int index, int generation )
			{
				m_index			= index;
				m_generation	= generation;
			}

			private readonly int m_index;
			private readonly int m_generation;


			#endregion // private
			////////////////////////////////////////////////////////////
		}


		#endregion // Handle 
		////////////////////////////////////////////////////////////
		// functions only callable with THandleMapShadowerKey == HandleMapShadower.HandleMapShadowerPrivateKey, which only HandleMap can access
		#region friend HandleMapShadower


		public static int GetHandleArrayIndex< THandleMapShadowerKey >( Handle handle ) where THandleMapShadowerKey : HandleMapShadowerBase, IKey => handle.GetHandleMapIndex< HandleMapPrivateKey >();

		public int GetSizeOfHandleArray< THandleMapShadowerKey >() where THandleMapShadowerKey : HandleMapShadowerBase, IKey => m_maxHandles;

		public void SubscribeHandleMapShadower< THandleMapShadowerKey >( Action< Handle > onJustAddedHandle, Action< Handle > onAboutToRemoveHandle )
		{ 
			EventJustAddedHandle		+= onJustAddedHandle;
			EventAboutToRemoveHandle	+= onAboutToRemoveHandle; 
		}

		public void UnsubscribeHandleMapShadower< THandleMapShadowerKey >( Action< Handle > onJustAddedHandle, Action< Handle > onAboutToRemoveHandle )
		{ 
			EventJustAddedHandle		-= onJustAddedHandle;
			EventAboutToRemoveHandle	-= onAboutToRemoveHandle; 
		}


		private event Action< Handle > EventJustAddedHandle;
		private event Action< Handle > EventAboutToRemoveHandle;


		#endregion//#region friend HandleMapShadower
		////////////////////////////////////////////////////////////
		#region protected


		protected static Handle CreateHandle( int index, int generation ) => Handle.CreateHandle< HandleMapPrivateKey >( index, generation );

		protected int GetSizeOfHandleArray() => m_maxHandles;

		protected static int GetHandleArrayIndex( Handle handle ) => handle.GetHandleMapIndex< HandleMapPrivateKey >();

		protected static int GetHandleGeneration( Handle handle ) => handle.GetHandleMapGeneration< HandleMapPrivateKey >();

		protected HandleMapBase( int maxHandles )
		{
			m_maxHandles = maxHandles;
		}
		
		protected void NotifyJustAddedHandle( Handle handle )		=> EventJustAddedHandle?.Invoke( handle );
		
		protected void NotifyAboutToRemoveHandle( Handle handle )	=> EventAboutToRemoveHandle?.Invoke( handle );


		#endregion// protected
		////////////////////////////////////////////////////////////
		#region private


		private readonly int m_maxHandles;


		#endregion// private
		////////////////////////////////////////////////////////////
		#region Handle friend HandleMapBase

		// uses technique described here: https://stackoverflow.com/questions/19135534/c-sharp-equivalent-to-c-friend-keyword/21518037
		// to emulate the C++ concept of "friend"	
		//
		// Part 3: private class which derives from both this class and the key interface
		//
		// note: there's actually nothing to stop someone just deriving from HandleMapBase & IKey to call the functions in the API in part 2 but
		// if we really cared we COULD make the functions throw if the generic param isn't HandleMapPrivateKey & then it would be runtime enforceable

		// HandleMap side of friend HandleMap "interface"
		private HandleMapBase() {}

		private	class HandleMapPrivateKey : HandleMapBase, IKey{}


		#endregion // Handle friend HandleMapBase
		////////////////////////////////////////////////////////////
	}


	// allows a HandleMap instance A to be "shodwed" 
	// so that Handles obtained from A can be used to store / retrieve values 
	// the shadowed map (which can't make its own handles)
	public abstract class HandleMapShadowerBase
	{
		////////////////////////////////////////////////////////////
		#region protected


		protected HandleMapShadowerBase( HandleMapBase toShadow )
		{
			ShadowedHandleMap = toShadow;
			ShadowedHandleMap.SubscribeHandleMapShadower< HandleMapShadowerPrivateKey >( OnJustAddedHandle, OnAboutToRemoveHandle );
		}

		~HandleMapShadowerBase()
		{
			ShadowedHandleMap.UnsubscribeHandleMapShadower< HandleMapShadowerPrivateKey >( OnJustAddedHandle, OnAboutToRemoveHandle );
		}

		protected int GetHandleMapShadowArraySize() => ShadowedHandleMap.GetSizeOfHandleArray< HandleMapShadowerPrivateKey >();

		protected int GetHandleMapShadowArrayIndex( HandleMapBase.Handle handle ) => HandleMapBase.GetHandleArrayIndex< HandleMapShadowerPrivateKey >( handle );


		protected virtual void OnJustAddedHandle	( HandleMapBase handleMap, HandleMapBase.Handle handle )	{}
		protected virtual void OnAboutToRemoveHandle( HandleMapBase handleMap, HandleMapBase.Handle handle )	{}


		#endregion //protected
		////////////////////////////////////////////////////////////
		#region private

																		
		protected HandleMapBase ShadowedHandleMap { get; private set; }

		private void OnJustAddedHandle( HandleMapBase.Handle handle )
		{
			OnJustAddedHandle( ShadowedHandleMap, handle );
		}

		private void OnAboutToRemoveHandle( HandleMapBase.Handle handle )
		{
			OnAboutToRemoveHandle( ShadowedHandleMap, handle );
		}
		
		
		#endregion// private
		////////////////////////////////////////////////////////////
		#region Handle friend HandleMapShadowerBase


		// HandleMapShadower side of friend HandleMapShadower "interface" for HandleMap
		private HandleMapShadowerBase() {}
		private	class HandleMapShadowerPrivateKey : HandleMapShadowerBase, IKey{}


		#endregion // Handle friend HandleMapShadowerBase
		////////////////////////////////////////////////////////////
	}


	//
	// generic HandleMap
	//
	public class HandleMap< T > : HandleMapBase
	{
		//----------------------------------------------------------
		public HandleMap( int maxNumItemsInHandleMap ) 
		: this ( maxNumItemsInHandleMap, EqualityComparer< T >.Default, default( T ) )   
		{}

		//----------------------------------------------------------
		public HandleMap( int maxNumItemsInHandleMap, EqualityComparer< T > customComparer ) 
		: this ( maxNumItemsInHandleMap, customComparer, default( T ) )   
		{}

		//----------------------------------------------------------
		public HandleMap( int maxNumItemsInHandleMap, T defaultValue )
		: this ( maxNumItemsInHandleMap, EqualityComparer< T >.Default, defaultValue )   
		{}

		//----------------------------------------------------------
		public HandleMap( int maxNumItemsInHandleMap, EqualityComparer< T > customComparer, T defaultValue ) 
		: base( maxNumItemsInHandleMap )
		{
			m_mappedItemComparer	= customComparer;
			m_defaultValue			= defaultValue;

			m_handleMappedArray		= new T				[ GetSizeOfHandleArray() ];
			m_indexGenerationArray	= new int			[ GetSizeOfHandleArray() ];
			m_freeIndexStack		= new Stack< int >	( GetSizeOfHandleArray() ); 
			m_mappedHandles			= new List< Handle >( GetSizeOfHandleArray() );

			for( int i = ( m_handleMappedArray.Length - 1 ); i >= 0; --i )
			{
				m_indexGenerationArray[ i ] = Handle.k_invalidHandleGeneration;
				m_freeIndexStack.Push( i );
			}
		}

		//----------------------------------------------------------
		public bool HandleHasValidMapping( Handle handle )
		{
			if( ! handle.IsInvalid() )
			{
				if( HasValidGeneration( handle ) )
				{
					return true;
				}
			}
			return false;
		}

		//----------------------------------------------------------
		public int	RemainingCapacity	=> m_freeIndexStack.Count;
		public bool IsFull				=> RemainingCapacity == 0;
		public int	Count				=> GetSizeOfHandleArray() - m_freeIndexStack.Count;

		//----------------------------------------------------------
		public Handle AddOrGetExistingHandle( T toAdd )
		{
			if( TryGetHandleForItem( toAdd, out var handleExisting ) )
			{
				return handleExisting;
			}
			else if( IsFull )
			{
				return Handle.Invalid;
			}

			var freeHandleIndex							= m_freeIndexStack.Pop();
			var indexGeneration							= IndexGenerationAdvance( freeHandleIndex );
			m_handleMappedArray[ freeHandleIndex ]		= toAdd;

			var handleAdded = CreateHandle( freeHandleIndex, indexGeneration );
			m_mappedHandles.Add( handleAdded );

			NotifyJustAddedHandle( handleAdded );
			return handleAdded;
		}

		//----------------------------------------------------------
		public bool TryGetMappedItem( Handle handle, out T potentiallyMappedItem )
		{
			if( HandleHasValidMapping( handle ) )
			{
				var index				= GetHandleArrayIndex( handle );
				potentiallyMappedItem	= m_handleMappedArray[ index ];
				return true;
			}

			potentiallyMappedItem = m_defaultValue;
			return false;
		}

		//----------------------------------------------------------
		public bool TryGetHandleForItem( T toFind, out Handle handle )
		{
			if( TryFindObject( toFind, out var index ) )
			{
				var generation	= IndexGenerationGetCurrent( index );
				handle			= CreateHandle( index, generation );

				return true;
			}

			handle = Handle.Invalid;
			return false;
		}

		//----------------------------------------------------------
		public void Remove( T toRemove )
		{
			if( TryGetHandleForItem( toRemove, out var handleToRemove ) )
			{
				Remove( handleToRemove );
			}
		}

		//----------------------------------------------------------
		public void Remove( Handle toRemove )
		{
			if( HandleHasValidMapping( toRemove ) )
			{
				NotifyAboutToRemoveHandle( toRemove );

				// invalidate objects and all existing handles - must be done before adding into the freestack so we can assert AdvanceIndexGeneration
				m_mappedHandles.Remove( toRemove );
				var mappedIndex							= GetHandleArrayIndex( toRemove );
				m_handleMappedArray		[ mappedIndex ] = m_defaultValue;
				IndexGenerationAdvance	( mappedIndex );
				m_freeIndexStack.Push	( mappedIndex );			
			}
		}

		//----------------------------------------------------------
		public void Clear()
		{
			while( m_mappedHandles.Count > 0 )
			{
				Remove( m_mappedHandles[ 0 ] );
			}

			m_freeIndexStack.Clear(); 
						
			for( int i = ( m_handleMappedArray.Length - 1 ); i >= 0; --i )
			{
				m_handleMappedArray[ i ] = m_defaultValue;
				IndexGenerationAdvance( i ); 
				m_freeIndexStack.Push( i );
			}
		}

		public ReadOnlyCollection< Handle > GetMappedHandles()
		{	
			return m_mappedHandles.AsReadOnly();
		}

		public void GetMappedItems( List< T > outListOfItems )
		{	
			for( int i = 0; i < m_handleMappedArray.Length; ++i )
			{
				if( m_handleMappedArray[ i ] != null )
				{
					outListOfItems.Add( m_handleMappedArray[ i ] );
				}
			}
		}

		////////////////////////////////////////////////////////////
		private readonly T[]					m_handleMappedArray		= null;
		private readonly int[]					m_indexGenerationArray	= null;
		private readonly Stack< int >			m_freeIndexStack		= null;
		private readonly List< Handle >			m_mappedHandles			= null;
		private readonly EqualityComparer< T >	m_mappedItemComparer	= null;
		private readonly T						m_defaultValue			= default( T );


		//----------------------------------------------------------
		private bool HasValidGeneration( Handle handle )
		{
			// assumes ( ! handle.IsInvalid() )
			var index		= GetHandleArrayIndex( handle );
			var generation	= GetHandleGeneration( handle );
			return	IndexGenerationIsValid( index, generation );
		}

		//----------------------------------------------------------
		private int IndexGenerationAdvance( int index ) =>

			// assert not in free stack
			++m_indexGenerationArray[ index ];

		//----------------------------------------------------------
		private int IndexGenerationGetCurrent( int index ) => m_indexGenerationArray[ index ];

		//----------------------------------------------------------
		private bool IndexGenerationIsValid( int index, int expectedIndexGeneration ) => m_indexGenerationArray[ index ] == expectedIndexGeneration;

		//----------------------------------------------------------
		private Handle CreateHandleFromIndex( int index ) => CreateHandle( index, IndexGenerationGetCurrent( index ) );

		//----------------------------------------------------------
		private bool TryFindObject( T toFind, out int foundAtIndex )
		{
			foundAtIndex = Handle.k_invalidHandleIndex;

			// assume non-null
			for( var i = 0; i < m_handleMappedArray.Length; ++i )
			{
				if( m_mappedItemComparer.Equals( toFind, m_handleMappedArray[ i ] ) )
				{
					foundAtIndex = i;
					return true;
				}
			}

			return false;
		}

	}

	//
	// HandleMapListener - subscribe to new handles being added old ones being removed
	//
	public abstract class HandleMapListener< TInSourceMap > : HandleMapShadowerBase	
	{
		protected HandleMapListener( HandleMap< TInSourceMap > handleMapToListenTo ) : base( handleMapToListenTo )
		{}

		public HandleMap< TInSourceMap > GetShadowedHandleMap()
		{
			return ShadowedHandleMap as HandleMap< TInSourceMap >;
		}

		public int Count => GetShadowedHandleMap().Count;
		public int RemainingCapacity => GetShadowedHandleMap().RemainingCapacity;
		public int RequiredMapArraySize => GetHandleMapShadowArraySize();


		protected sealed override void OnJustAddedHandle( HandleMapBase handleMap, HandleMapBase.Handle handle )
		{
			OnJustAddedHandle( (HandleMap< TInSourceMap >) handleMap, handle );
		}

		protected sealed override void OnAboutToRemoveHandle( HandleMapBase handleMap, HandleMapBase.Handle handle )
		{
			OnAboutToRemoveHandle( (HandleMap< TInSourceMap >) handleMap, handle );
		}

		protected abstract void OnJustAddedHandle		( HandleMap< TInSourceMap > handleMap, HandleMapBase.Handle handle );
		protected abstract void OnAboutToRemoveHandle	( HandleMap< TInSourceMap > handleMap, HandleMapBase.Handle handle );
	}


	public abstract class HandleMapShadower< THandleMapped, TShadowed > : HandleMapShadowerBase
	{
		protected HandleMapShadower( HandleMapBase handleMapToShadow ) 
		: this ( handleMapToShadow, default( TShadowed ) )
		{}

		protected HandleMapShadower( HandleMapBase handleMapToShadow, TShadowed defaultValue ) 
		: base( handleMapToShadow )
		{
			m_handleMapShadowArray = new TShadowed[ GetHandleMapShadowArraySize() ];
			m_defaultShadowedValue = defaultValue;
		}

		public HandleMap< THandleMapped > GetShadowedHandleMap()
		{
			return ShadowedHandleMap as HandleMap< THandleMapped >;
		}

		public int Count                => GetShadowedHandleMap().Count;
		public int RemainingCapacity    => GetShadowedHandleMap().RemainingCapacity;
		public int RequiredMapArraySize => GetHandleMapShadowArraySize();

		public int GetShadowArrayIndexForHandle( HandleMapBase.Handle handle )
		{
			if( ((HandleMap< THandleMapped >) ShadowedHandleMap ).HandleHasValidMapping( handle ) )
			{
				return GetHandleMapShadowArrayIndex( handle );
			}
			return Constants.k_iInvalidIndex;
		}

		// is this a good idea?!?
		public bool TryGetShadowed( HandleMapBase.Handle handle, out TShadowed shadowed )
		{
			// cast is safe because is was set from the same type in the constructor
			if( ((HandleMap< THandleMapped >) ShadowedHandleMap ).HandleHasValidMapping( handle ) )
			{
				var shadowedIndex	= GetHandleMapShadowArrayIndex( handle );
				shadowed			= m_handleMapShadowArray[ shadowedIndex ];
				return true;
			}

			shadowed = m_defaultShadowedValue;
			return false;
		}

		// is this a good idea?!?
		protected sealed override void OnJustAddedHandle( HandleMapBase handleMap, HandleMapBase.Handle handle )
		{
			var shadowedIndex = GetHandleMapShadowArrayIndex( handle );
			OnJustAddedHandle( (HandleMap< THandleMapped >) handleMap, handle, ref m_handleMapShadowArray[ shadowedIndex ] );
		}

		// is this a good idea?!?
		protected sealed override void OnAboutToRemoveHandle( HandleMapBase handleMap, HandleMapBase.Handle handle )
		{
			var shadowedIndex = GetHandleMapShadowArrayIndex( handle );
			OnAboutToRemoveHandle( (HandleMap< THandleMapped >) handleMap, handle, ref m_handleMapShadowArray[ shadowedIndex ] );
		}

		protected abstract void OnJustAddedHandle		( HandleMap< THandleMapped > handleMap, HandleMapBase.Handle handle, ref TShadowed refToShadowedInstanceForHandle );
		protected abstract void OnAboutToRemoveHandle	( HandleMap< THandleMapped > handleMap, HandleMapBase.Handle handle, ref TShadowed refToShadowedInstanceForHandle );

		protected	TShadowed[] m_handleMapShadowArray = null;
		protected	TShadowed	m_defaultShadowedValue = default( TShadowed );
	}
}
